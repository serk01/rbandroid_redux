/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

import java.util.EmptyStackException;

import apps.serk01.rbandroid.R;
import apps.serk01.rbandroid.configuration.Constants;
import apps.serk01.rbandroid.fragments.BaseFragment;
import apps.serk01.rbandroid.fragments.BloggerListFragment;
import apps.serk01.rbandroid.fragments.FavoritesListFragment;
import apps.serk01.rbandroid.fragments.NavigationDrawerFragment;
import apps.serk01.rbandroid.fragments.SchedulePagerFragment;
import apps.serk01.rbandroid.fragments.SpeakerFragment;
import apps.serk01.rbandroid.utils.PrefManager;
import apps.serk01.rbandroid.utils.Utilities;

public class MainActivity extends BaseActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, BaseFragment.OnFragmentInteractionListener {

    private static final String TAG = MainActivity.class.getCanonicalName();
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);


        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        //set the title to the default fragments name
        Constants.FragmentName frType = Constants.FragmentName.values()[0];
        mTitle = Utilities.getInstance().GetBlogPageTitleForFragment(MainActivity.this, frType);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        checkForUpdates();
    }

    public void onHowToCheckboxClicked(View view) {
        boolean isChecked = ((CheckBox) view).isChecked();
        if (isChecked) {
            PrefManager.getInstance().setShowHowToScreen(false, getApplicationContext());
        }
    }

    public void onHowToDoneClicked(View view) {
        View howToView = findViewById(R.id.howtoview);
        howToView.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        int fragmentCount = fm.getBackStackEntryCount();

        //If we only have one fragment at the Back stack, pressing Back should take as out of the app
        if (fragmentCount == 1)
            this.finish();

        //else, we just go back on the stack
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStackImmediate();
            setActionBarTitle(getTopFragmentTitle());
            try {
                mNavigationDrawerFragment.popFromPositionStack();
            } catch (EmptyStackException e) {
                Log.d(TAG, "empty stack exception");
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private String getTopFragmentTitle(){
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry topBackStackEntry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);
            return topBackStackEntry.getName();
        }

        //default to the first fragment type
        return Utilities.getInstance().GetBlogPageTitleForFragment(MainActivity.this, Constants.FragmentName.kFragLive);
    }

    @Override
    public boolean onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        Constants.FragmentName section = Constants.FragmentName.values()[position];

        String name = Utilities.getInstance().GetBlogPageTitleForFragment(MainActivity.this, section);
        int backStackSize = fragmentManager.getBackStackEntryCount();

        if (backStackSize > 0) {
            String topFragmentName = getTopFragmentTitle();
            if (topFragmentName.equals(name))
                return false;
        }

        switch (section) {
            case kFragLive:
                fragmentManager.beginTransaction().
                        replace(R.id.fragmentsContainer, SpeakerFragment.newInstance(position, Constants.FragmentType.kFragTypeSpeaker, SpeakerFragment.class))
                        .addToBackStack(name)
                        .commit();
                return true;
            case kFragMain:
            case kFragNews:
            case kFragBlog:
            case kFragEkpombes:
                fragmentManager.beginTransaction()
                        .replace(R.id.fragmentsContainer, BloggerListFragment.newInstance(position, Constants.FragmentType.kFragTypeBlogPost, BloggerListFragment.class))
                        .addToBackStack(name)
                        .commit();
                return true;
            case kFragSchedule:
                fragmentManager.beginTransaction()
                        .replace(R.id.fragmentsContainer, SchedulePagerFragment.newInstance(position, Constants.FragmentType.kFragTypeViewPager, SchedulePagerFragment.class))
                        .addToBackStack(name)
                        .commit();
                return true;
            case kFragFavorites:
                fragmentManager.beginTransaction()
                        .replace(R.id.fragmentsContainer, FavoritesListFragment.newInstance(position, Constants.FragmentType.kFragTypeFavorites, FavoritesListFragment.class))
                        .addToBackStack(name)
                        .commit();
                return true;

        }
        return false;
    }

    public void onSectionAttached(int number) {
        Constants.FragmentName frType = Constants.FragmentName.values()[number];
        mTitle= Utilities.getInstance().GetBlogPageTitleForFragment(MainActivity.this, frType);
//        setActionBarTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
//            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

//    /**
//     * A placeholder fragment containing a simple view.
//     */
//    public static class PlaceholderFragment extends Fragment {
//        /**
//         * The fragment argument representing the section number for this
//         * fragment.
//         */
//        private static final String ARG_SECTION_NUMBER = "section_number";
//
//        /**
//         * Returns a new instance of this fragment for the given section
//         * number.
//         */
//        public static PlaceholderFragment newInstance(int sectionNumber) {
//            PlaceholderFragment fragment = new PlaceholderFragment();
//            Bundle args = new Bundle();
//            fragment.setArguments(args);
//            return fragment;
//        }
//
//        public PlaceholderFragment() {
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//
//            //default view. make sure to cover all scenarios in the switch below
//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            return rootView;
//        }
//
//        @Override
//        public void onAttach(Activity activity) {
//            super.onAttach(activity);
//            ((MainActivity) activity).onSectionAttached(
//                    getArguments().getInt(ARG_SECTION_NUMBER));
//        }
//    }

}
