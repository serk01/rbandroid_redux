/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import apps.serk01.rbandroid.R;
import apps.serk01.rbandroid.base_classes.MyWebview;
import apps.serk01.rbandroid.configuration.Constants;
import apps.serk01.rbandroid.daogen.BloggerPost;
import apps.serk01.rbandroid.database.Database;
import apps.serk01.rbandroid.listeners.WebResponseListener;
import apps.serk01.rbandroid.net.NetOperations;
import apps.serk01.rbandroid.utils.Utilities;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by serk01 on 11/11/14.
 */
public class DetailsActivity extends BaseActivity implements WebResponseListener {

    private static final String TAG = DetailsActivity.class.getCanonicalName();
    private TextView mTitleView;
    private TextView mUploaderField;
    private TextView mDateField;
    private MyWebview mWebView;
    private static final String MXC_MESSAGE = "</br></br><i>[A link for this mixcloud stream should be available on the action bar]</i></br></br>";
    private BloggerPost mBloggerPost;

    private List<String> mMxcIframes = new ArrayList<String>();
    private List<String> mDownloadLinks = new ArrayList<String>();
    private String mPostId;

    private MenuItem mLinkMenuItem;
    private MenuItem mFavMenuItem;
    private ImageView mBannerView;
    private int mLogoResourceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        mPostId = extras.getString("postId");
        mBloggerPost = getLatestVersionOfPostFromDB();

        Constants.FragmentName frType = Constants.FragmentName.values()[extras.getInt("fragmentType")];
        mTitle = Utilities.getInstance().GetBlogPageTitleForFragment(DetailsActivity.this, frType);
        restoreActionBar();

        detectContextAndSetContentView();


        if (!Utilities.getInstance().isConnected(DetailsActivity.this)) {
            Crouton.makeText(this, getResources().getString(R.string.noconnection), Style.ALERT).show();
        }

        String content = mBloggerPost.getContent();

        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><link href=\"style.css\" type=\"text/css\" rel=\"stylesheet\"/></head><body>");
        sb.append(content);
        sb.append("</body></html>");

        loadContentInWebview(sb.toString());

        parseContentForMxc();
        parseContentForYoutube();

    }

    private void setup() {
        //enclose everything in try-catch blocks because depending on the contentView some
        //widgets may not exists

        try {
            mTitleView = (TextView) findViewById(R.id.details_title);
            mTitleView.setText(mBloggerPost.getTitle());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mBannerView = (ImageView) findViewById(R.id.details_banner);
            mBannerView.setImageResource(mLogoResourceId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mUploaderField = (TextView) findViewById(R.id.details_uploader);
            mUploaderField.setText(getResources().getString(R.string.uploaded_by) + " " + mBloggerPost.getAuthorName());
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        try {
            mDateField = (TextView) findViewById(R.id.details_date);
            mDateField.setText(Utilities.getInstance().GetVerySimpleDateFormat(mBloggerPost.getDatePublished()));
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            mWebView = (MyWebview) findViewById(R.id.details_webview);
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.setWebChromeClient(new WebChromeClient()); //necessary to play embedded youtube
            mWebView.setWebViewClient(new MyWebviewClient());

            mWebView.setBackgroundColor(Color.argb(1, 0, 0, 0)); //this fixes the flickering problem
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        setup();
    }

    private boolean hasBanner() {
        mLogoResourceId = R.drawable.radiobubble_logo;
        boolean logoFound = false;

        JSONArray labelsArray = mBloggerPost.getLabelsAsArray();
        if (labelsArray != null) {
            for (int i = 0; i < labelsArray.length(); i++) {
                String label = null;
                try {
                    label = labelsArray.getString(i);
                    Integer resourceId = Constants.logosMap.get(label);
                    if (resourceId != null) {
                        mLogoResourceId = resourceId.intValue();
                        logoFound = true;
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return logoFound;
    }
    private void detectContextAndSetContentView() {
        if (hasBanner())
            setContentView(R.layout.activity_details_withlogo);
        else
            setContentView(R.layout.activity_details_withoutlogo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Only show items in the action bar relevant to this screen
        // if the drawer is not showing. Otherwise, let the drawer
        // decide what to show in the action bar.
        getMenuInflater().inflate(R.menu.details, menu);

        mLinkMenuItem = menu.findItem(R.id.menu_item_link);
        mFavMenuItem = menu.findItem(R.id.menu_item_favorite);

        if (mBloggerPost != null && mBloggerPost.getDownloadLinks() != null)
            revealLinkMenuItem();

        setFavMenuItemIcon();

        restoreActionBar();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.menu_item_favorite) {
            mBloggerPost.setIsFavorite(!mBloggerPost.getIsFavorite()); //toggle favorite state
            updatePostInDatabase();
            setFavMenuItemIcon();
            return true;
        }
        else if (id == R.id.menu_item_share) {
            sharePost();
            return true;
        }

        else if (id == R.id.menu_item_link) {
            openStreamLink();
        }

        return super.onOptionsItemSelected(item);
    }

    private void openStreamLink() {
        Mint.logEvent("open stream link pressed", MintLogLevel.Info);

        Log.d(TAG,"Mixcloud is installed? " + Utilities.getInstance().isAppInstalled(DetailsActivity.this, "com.mixcloud.player"));

        try {
            Intent mixcloudIntent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.parse("mc://www.mixcloud.com/" + mBloggerPost.getMixcloudUrl());
            mixcloudIntent.setData(uri);
            startActivity(mixcloudIntent);
        } catch (ActivityNotFoundException e) {
            Intent mixcloudIntent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.parse("http://www.mixcloud.com/"+mBloggerPost.getMixcloudUrl());
            mixcloudIntent.setData(uri);
            startActivity(mixcloudIntent);
        }
    }

    private void sharePost() {
        Mint.logEvent("share post pressed", MintLogLevel.Info);

        StringBuilder shareText = new StringBuilder();
        shareText.append(mBloggerPost.getTitle());
        shareText.append(" - ");
        shareText.append(mBloggerPost.getUrl());
        shareText.append(" - via #rbandroid");

        Utilities.getInstance().spawnChooser(DetailsActivity.this, shareText.toString(), mBloggerPost.getTitle());
    }


    private void setFavMenuItemIcon() {
        if (mBloggerPost.getIsFavorite()) {
            mFavMenuItem.setIcon(getResources().getDrawable(android.R.drawable.btn_star_big_on));
            Mint.logEvent("favourite pressed", MintLogLevel.Info);
        }
        else
            mFavMenuItem.setIcon(getResources().getDrawable(android.R.drawable.btn_star_big_off));
    }

    private void revealLinkMenuItem() {
        mLinkMenuItem.setVisible(true);
    }


    // Since this activity makes changes to the post content depending on its context
    // We use this helper to always get the most updated version of the blog post from the database
    private BloggerPost getLatestVersionOfPostFromDB() {
        return Database.getInstance(this).GetBloogerPostWithId(mPostId);
    }


    private void loadContentInWebview(String html) {
        mWebView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
    }

    private void renderUpdatedContent() {
        String content = mBloggerPost.getContent();

        loadContentInWebview(content);

    }

    private void updatePostInDatabase() {
        Database.getInstance(this).UpdatePost(mBloggerPost);
    }

    private void updatePostDownloadLinks() {
        mBloggerPost.setDownloadLinks(mDownloadLinks.toString());
        Database.getInstance(this).UpdatePost(mBloggerPost);
    }

    private Boolean hasMxcContent(String content) {
        return content.contains("mixcloud");
    }

    private void parseContentForMxc() {
        mBloggerPost = getLatestVersionOfPostFromDB();
        String content = mBloggerPost.getContent();

        //if this post has not be parsed for mxc content
        if (!mBloggerPost.getHasBeenParsed() && hasMxcContent(content)) {
            getMxcIframeSources(content);
        }

        //else if it's been parsed but couldn't get download links, try again
        else if (mBloggerPost.getHasBeenParsed() && mBloggerPost.getMixcloudIframes() != null && mBloggerPost.getDownloadLinks() == null) {
            String storedIframeSourcesStr = mBloggerPost.getMixcloudIframes();
            if (storedIframeSourcesStr.length() > 0) {
                String[] storedIframeSources = storedIframeSourcesStr.split(",");
                for (String iframeSource : storedIframeSources)
                    mMxcIframes.add(iframeSource);
            }
            getMxcDownloadLink();
        } else
            Log.d(TAG, " this post has been parsed");

    }

    /**
     * Parses the content of this mBloggerPost and looks for Mxc links. If it finds any
     * it extracts the iframe source attribute and stores it in the blog post object
     *
     * @param content The post content
     */
    private void getMxcIframeSources(String content) {
        Document contentDoc = Jsoup.parse(content);
        Elements iframeElements = contentDoc.getElementsByTag("iframe");

        for (Element el : iframeElements) {
            String iframeSource = el.attr("src");
            if (!iframeSource.startsWith("http:") && !iframeSource.startsWith("https:")) {
                el.attr("src", "http:" + iframeSource);
                mBloggerPost.setContent(contentDoc.toString());
            }

            //check if this is a mixcloud iframe, we might have other iframes in the content as well
            if (iframeSource.contains("mixcloud")) {
                mMxcIframes.add(iframeSource);
            }
            mBloggerPost.setMixcloudIframes(mMxcIframes.toString());

        }

        mBloggerPost.setHasBeenParsed(true);
        updatePostInDatabase();
        renderUpdatedContent();
        getMxcDownloadLink();
    }

    /**
     * Goes through the mxc iframe source that have been collected and generates a url which is used
     * to ask for extra details for a mixcloud stream
     * example link to get details https://www.mixcloud.com/player/details/?key=/rbupload/treis_laloun_2015-01-05/

     */

    private void getMxcDownloadLink() {
        try {
            for (String iframeSource : mMxcIframes) {
                int indexOfFeedString = iframeSource.indexOf("feed=");
                iframeSource = iframeSource.substring(indexOfFeedString);
                int indexOfAmpersandString = iframeSource.indexOf("&");
                iframeSource = iframeSource.substring(0,indexOfAmpersandString);
//                iframeSource = iframeSource.substring(iframeSource.indexOf("feed="), iframeSource.indexOf("&"));
                iframeSource = URLDecoder.decode(iframeSource);
                iframeSource = iframeSource.replace("feed=", ""); //remove this part
                Log.d(TAG, "src:" + iframeSource);

                //find the injection point
                String endString = ".com";
                int urlBaseIndexEnd = iframeSource.indexOf(endString);

                //use mixclouds details API to get information about the stream
                StringBuilder sb = new StringBuilder(iframeSource);
                sb.insert(urlBaseIndexEnd + endString.length(), Constants.MIXLCOUD_GET_DETAULS_SUFFIX);
                String mxcDetailsUrl = sb.toString();
                Log.d(TAG, "url:" + mxcDetailsUrl);
                putRequestForMxcDetails(mxcDetailsUrl);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void putRequestForMxcDetails(String url) {
        //request the details of this stream
        NetOperations.getInstance(this).GetJSONResultForURL(url, this, NetOperations.REQUEST_TYPE.kMxcRequest);
    }

    /**
     * After we have the details of a Mxc stream returned, this method
     * parses the waveformURL field to generate a direct download link
     *
     * @param mxcResponse the JSONObject that we received from the mxc API call
     */
    private void setMxcloudStreamDetails(JSONObject mxcResponse) {
        //create the mp3 file

        try {
            String waveformUrl = mxcResponse.getString("waveform_url");
            waveformUrl = waveformUrl.replace("json", "mp3");

            String endString = ".com";
            int urlBaseIndexEnd = waveformUrl.indexOf(endString);

            StringBuilder sb = new StringBuilder(waveformUrl);
            sb.replace(0, urlBaseIndexEnd + endString.length(), "stream20.mixcloud.com/c/originals");
            sb.insert(0, "http://");
            Log.d(TAG, "mixcloud download link :" + sb.toString());
            mDownloadLinks.add(sb.toString());
            updatePostDownloadLinks();
            revealLinkMenuItem();
//            Crouton.makeText(this, sb.toString(), Style.INFO).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //set the mxcld stream url
        try {
            mBloggerPost.setMixcloudUrl(mxcResponse.getJSONObject("cloudcast").getString("url"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        updatePostInDatabase();
    }

    private void parseContentForYoutube() {
        mBloggerPost = getLatestVersionOfPostFromDB();

        Document contentDoc = Jsoup.parse(mBloggerPost.getContent());
        Elements iframeElements = contentDoc.getElementsByTag("iframe");
        for (Element el : iframeElements) {
            String iframeSource = el.attr("src");

            //check if this is a mixcloud iframe, we might have other iframes in the content as well
            if (iframeSource.contains("youtube") && !(iframeSource.contains("http"))) {
                StringBuilder sb = new StringBuilder(iframeSource);
                sb.insert(0, "http:");
                el.attr("src", sb.toString()); //update the attribute
                mBloggerPost.setContent(contentDoc.toString()); //update the blog post content
                renderUpdatedContent(); //render the updated content
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        Crouton.cancelAllCroutons();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void successfullNetOperation(JSONObject response, NetOperations.REQUEST_TYPE requestType) {
        if (response != null) {
            if (requestType == NetOperations.REQUEST_TYPE.kMxcRequest) {
                String waveformUrl;
                setMxcloudStreamDetails(response);
            }
        }
    }

    @Override
    public void failedNetOperation(VolleyError error) {
        if (error != null && error.networkResponse != null) {
            //301 Moved Permanently
            if (error.networkResponse.statusCode == 301) {
                String newLocation = error.networkResponse.headers.get("Location");
                putRequestForMxcDetails(newLocation);

            }
        }
    }

    public class MyWebviewClient extends WebViewClient {
        public MyWebviewClient() {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }
}