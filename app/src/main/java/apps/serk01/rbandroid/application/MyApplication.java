/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.application;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.splunk.mint.Mint;

import io.fabric.sdk.android.Fabric;

/**
 * Created by serk01 on 02/02/15.
 */
public class MyApplication extends Application {

    private static final String TAG = MyApplication.class.getCanonicalName();

    @Override
    public void onCreate() {
        super.onCreate();

        Mint.initAndStartSession(getApplicationContext(), "19d636a9");

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();

        Fabric.with(fabric);
    }
}