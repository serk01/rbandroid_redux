/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/***
    __author__ ="Serko Katsikian"
    __date__ = "2012"
    __description__ = "radiobubble.gr android app"
    __copyleft__ = "Copyleft -all rights reversed- 2012 Serko Katsikian"
    __license__ = "GPLv3"

    Radiobubble-Android: An android app for the webradio station www.radiobubble.gr
        
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Feature suggestions, complaints or bug reports,
    please direct them to serko.apps@gmail.com 
*/
package apps.serk01.rbandroid.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import apps.serk01.rbandroid.mediaplayer.LiveStreamController;

public class OutgoingCallReceiver extends BroadcastReceiver {

	private static final String TAG = OutgoingCallReceiver.class.getName();
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		
		if(null == bundle)
			return;
		
		String phonenumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
		String info = "Detect Calls sample application\nOutgoing number: " + phonenumber;


//		if (MediaController.instance().isPlaying()) {
////			if (!RbMediaPlayer.isLiveStream)
//			if (MediaController.instance().mode != MODE.LIVESTREAM)
//				MediaController.instance().pausePlayer();
//			else
//				MediaController.instance().mutePlayer();
//		}

        if (LiveStreamController.getInstance().isPlaying()) {
            LiveStreamController.getInstance().mutePlayer(context);
        }
	}
}