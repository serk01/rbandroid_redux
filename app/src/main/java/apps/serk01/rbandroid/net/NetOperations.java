/*

 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.net;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import apps.serk01.rbandroid.configuration.Constants;
import apps.serk01.rbandroid.listeners.WebResponseListener;

/**
 * Created by serk01 on 02/11/14.
 */
public class NetOperations {

    private static final String TAG = NetOperations.class.getCanonicalName();
    private static NetOperations instance;
    private Context context;
    private int BLOGGER_MAX_RESULTS = 15;

    public static enum REQUEST_TYPE {kBloggerRequest, kMxcRequest};

    private NetOperations(Context context) {

        this.context = context;
    }

    /**
     * Generates a url to retrieve blogger posts starting from a specific date and onwwards
     *
     * @param pageID The blogger page Id
     * @param startDate The published date of the post from which we start looking
     * @param listener The response listener
     */
    public void GetPostsForPageIdWithStartDate(String pageID, String startDate, final WebResponseListener listener) {

        String url = String.format(Constants.BLOGGER_BLOG_POSTS_URL_WITH_START_DATE, pageID, BLOGGER_MAX_RESULTS, startDate);
        createRequest(url, listener, REQUEST_TYPE.kBloggerRequest);
    }

    /**
     * Generates a url to retrieve the next page of blog posts using the nextPageToken of the
     * previously retrieved posts
     *
     * @param pageID The blogger page Id
     * @param nextPageToken The next page token to use in our request
     * @param listener The response listener
     */
    public void GetPostsForPageIdWithNextPageToken(String pageID, String nextPageToken, final WebResponseListener listener) {

        String url = String.format(Constants.BLOGGER_BLOG_POSTS_URL_NEXT_PAGE, pageID, BLOGGER_MAX_RESULTS, nextPageToken);
        createRequest(url, listener, REQUEST_TYPE.kBloggerRequest);
    }

    /**
     * Generates a standard url without any extra parameters to retrieve the latest number of posts
     *
     * @param pageID The blogger page Id
     * @param listener The response listener
     */
    public void GetPostsForPageId(String pageID, final WebResponseListener listener) {

        String url = String.format(Constants.BLOGGER_BLOG_POSTS_URL, pageID, BLOGGER_MAX_RESULTS);
        createRequest(url, listener, REQUEST_TYPE.kBloggerRequest);
    }

    public void GetJSONResultForURL(String url, WebResponseListener listener, REQUEST_TYPE requestType) {
        createRequest(url ,listener, requestType);
    }

    /**
     * Generates the Request object that is added to Volley's queue and returns a JSONObject response
     * to the attached listener
     *
     * @param url The url to retrieved data from
     * @param listener The response listener
     */
    private void createRequest(String url, final WebResponseListener listener, final REQUEST_TYPE requestType) {
        Log.d(TAG, "Request url:" + url);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG,"volley-success:"+response);
                        listener.successfullNetOperation(response, requestType);

                    }
                },
                        new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.failedNetOperation(error);
                        // TODO Auto-generated method stub

                    }
                });

        RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
        queue.add(jsObjRequest);

    }

    public static NetOperations getInstance(Context context) {
        if (instance == null) {
            instance = new NetOperations(context);
        }

        return instance;
    }
}
