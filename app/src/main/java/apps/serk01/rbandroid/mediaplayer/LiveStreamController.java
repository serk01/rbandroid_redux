/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.mediaplayer;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;

import apps.serk01.rbandroid.listeners.MediaControllerEventsListener;
import apps.serk01.rbandroid.utils.PrefManager;

import static android.media.MediaPlayer.OnPreparedListener;

/**
 * Created by serk01 on 28/12/14.
 */
public class LiveStreamController extends MediaPlayer implements OnPreparedListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {

    public int mBufferedPercent;
    private static LiveStreamController instance = new LiveStreamController();
    private MediaControllerEventsListener mEventsListener;
    private String TAG = LiveStreamController.class.getCanonicalName();
    private boolean mIsPreparing;
    private boolean isMute;

    public static LiveStreamController getInstance() {
        if (instance == null) {
            instance = new LiveStreamController();
        }
        return instance;
    }

    private LiveStreamController() {

    }


    public void toggleLiveStream(MediaControllerEventsListener listener, Context context) {
        mEventsListener = listener;
        if (!this.isPlaying())
            try {
                startStreaming(context);
            } catch (Exception e) { //if user tries to toggle while still preparing, this will avoid crashing the app
                e.printStackTrace();
            }
        else
            stopStreaming();
    }

    private void startStreaming(Context context) {

        setOnPreparedListener(this);
        setOnBufferingUpdateListener(this);
        setOnErrorListener(this);
        setOnCompletionListener(this);

        String url = PrefManager.getInstance().getStramUrl(context);
        Log.d(TAG, "Stream Url:" + url);

        this.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            this.setDataSource(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.prepareAsync(); // might take long! (for buffering, etc)
        mIsPreparing = true;
        mEventsListener.isPreparing();

    }

    private void stopStreaming() {
        this.stop();
        this.reset();
        mEventsListener.stoppedStreaming();
    }

    public void mutePlayer(Context context) {
        isMute = true;
        AudioManager audio = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        Log.d(TAG,"currentVolume:"+currentVolume);
        audio.setStreamMute(AudioManager.STREAM_MUSIC, true);
    }

    public void unmutePlayer(Context context) {
        isMute = false;
        AudioManager audio = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        Log.d(TAG,"currentVolume:"+currentVolume);
        audio.setStreamMute(AudioManager.STREAM_MUSIC, false);
    }

    public boolean isMute() {
        return isMute;
    }

    public boolean isPreparing() {
        return mIsPreparing;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mIsPreparing = false;
        Log.d(TAG,"Media controller ready");
        if (!this.isPlaying()) {
            this.start();
            mEventsListener.startedStreaming();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        mBufferedPercent = percent;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG,"Error with code" + what);
        Log.d(TAG,"Could not play stream, is the stream url correct?");
        mEventsListener.stoppedStreaming(); //to hide the progress bar
        this.reset();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG,"completed");
        this.reset();
    }
}
