package apps.serk01.rbandroid.daogen;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
// KEEP INCLUDES END
/**
 * Entity mapped to table BLOGGER_POST.
 */
public class BloggerPost implements java.io.Serializable {

    private String id;
    private String title;
    private String content;
    private String url;
    private String authorName;
    private String authorProfileUrl;
    private String authorImageUrl;
    private java.util.Date datePublished;
    private String datePublishedString;
    private java.util.Date dateUpdated;
    private String dateUpdatedString;
    private String blogPageId;
    private String blogPageTitle;
    private String nextPageToken;
    private Boolean hasBeenParsed;
    private Boolean isFavorite;
    private String downloadLinks;
    private String mixcloudIframes;
    private String mixcloudUrl;
    private String labels;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public BloggerPost() {
    }

    public BloggerPost(String id) {
        this.id = id;
    }

    public BloggerPost(String id, String title, String content, String url, String authorName, String authorProfileUrl, String authorImageUrl, java.util.Date datePublished, String datePublishedString, java.util.Date dateUpdated, String dateUpdatedString, String blogPageId, String blogPageTitle, String nextPageToken, Boolean hasBeenParsed, Boolean isFavorite, String downloadLinks, String mixcloudIframes, String mixcloudUrl, String labels) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.url = url;
        this.authorName = authorName;
        this.authorProfileUrl = authorProfileUrl;
        this.authorImageUrl = authorImageUrl;
        this.datePublished = datePublished;
        this.datePublishedString = datePublishedString;
        this.dateUpdated = dateUpdated;
        this.dateUpdatedString = dateUpdatedString;
        this.blogPageId = blogPageId;
        this.blogPageTitle = blogPageTitle;
        this.nextPageToken = nextPageToken;
        this.hasBeenParsed = hasBeenParsed;
        this.isFavorite = isFavorite;
        this.downloadLinks = downloadLinks;
        this.mixcloudIframes = mixcloudIframes;
        this.mixcloudUrl = mixcloudUrl;
        this.labels = labels;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorProfileUrl() {
        return authorProfileUrl;
    }

    public void setAuthorProfileUrl(String authorProfileUrl) {
        this.authorProfileUrl = authorProfileUrl;
    }

    public String getAuthorImageUrl() {
        return authorImageUrl;
    }

    public void setAuthorImageUrl(String authorImageUrl) {
        this.authorImageUrl = authorImageUrl;
    }

    public java.util.Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(java.util.Date datePublished) {
        this.datePublished = datePublished;
    }

    public String getDatePublishedString() {
        return datePublishedString;
    }

    public void setDatePublishedString(String datePublishedString) {
        this.datePublishedString = datePublishedString;
    }

    public java.util.Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(java.util.Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getDateUpdatedString() {
        return dateUpdatedString;
    }

    public void setDateUpdatedString(String dateUpdatedString) {
        this.dateUpdatedString = dateUpdatedString;
    }

    public String getBlogPageId() {
        return blogPageId;
    }

    public void setBlogPageId(String blogPageId) {
        this.blogPageId = blogPageId;
    }

    public String getBlogPageTitle() {
        return blogPageTitle;
    }

    public void setBlogPageTitle(String blogPageTitle) {
        this.blogPageTitle = blogPageTitle;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public Boolean getHasBeenParsed() {
        return hasBeenParsed;
    }

    public void setHasBeenParsed(Boolean hasBeenParsed) {
        this.hasBeenParsed = hasBeenParsed;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getDownloadLinks() {
        return downloadLinks;
    }

    public void setDownloadLinks(String downloadLinks) {
        this.downloadLinks = downloadLinks;
    }

    public String getMixcloudIframes() {
        return mixcloudIframes;
    }

    public void setMixcloudIframes(String mixcloudIframes) {
        this.mixcloudIframes = mixcloudIframes;
    }

    public String getMixcloudUrl() {
        return mixcloudUrl;
    }

    public void setMixcloudUrl(String mixcloudUrl) {
        this.mixcloudUrl = mixcloudUrl;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    // KEEP METHODS - put your custom methods here
    public void setFieldsFromJSONObject(JSONObject postJSON, String pageTitle, String nextPageToken) throws JSONException {
        setId(postJSON.getString("id"));
        setBlogPageId(postJSON.getJSONObject("blog").getString("id"));
        setBlogPageTitle(pageTitle);
        setTitle(postJSON.getString("title"));
        setContent(postJSON.getString("content"));
        setUrl(postJSON.getString("url"));
        setAuthorName(postJSON.getJSONObject("author").getString("displayName"));
        setAuthorImageUrl(postJSON.getJSONObject("author").getJSONObject("image").getString("url"));
        setAuthorProfileUrl(postJSON.getJSONObject("author").getString("url"));
        setDatePublished(getDateFromString(postJSON.getString("published")));
        setDatePublishedString(postJSON.getString("published"));
        setDateUpdated(getDateFromString(postJSON.getString("updated")));
        setDateUpdatedString(postJSON.getString("updated"));
        try {
            setLabels(postJSON.getJSONArray("labels").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setNextPageToken(nextPageToken);
        setHasBeenParsed(false);
        setIsFavorite(false);

    }

    private Date getDateFromString(String dateStr) {
//        incoming date format --> 2014-10-24T10:59:25+03:00
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date date = format.parse(dateStr);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


    public JSONArray getLabelsAsArray() {
        String labels = getLabels();
        JSONArray labelsArray = null;
        try {
            labelsArray = new JSONArray(labels);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return labelsArray;
    }

    // KEEP METHODS END

}
