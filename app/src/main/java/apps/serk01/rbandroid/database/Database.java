/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package apps.serk01.rbandroid.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;

import apps.serk01.rbandroid.daogen.BloggerPost;
import apps.serk01.rbandroid.daogen.BloggerPostDao;
import apps.serk01.rbandroid.daogen.DaoMaster;
import apps.serk01.rbandroid.daogen.DaoSession;

/**
 * Created by serk01 on 04/11/14.
 */

public class Database {

    private static final String TAG = Database.class.getCanonicalName();
    private String DB_NAME = "rbandroid-db.sqlite";
    public static int FETCH_LIMIT = 10;

    private static Database instance;

    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private SQLiteDatabase db;
    private BloggerPostDao bloggerPostDao;

    private Database(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, DB_NAME, null);
        daoMaster = new DaoMaster(db);
        db = helper.getWritableDatabase();
        Log.d(TAG,"Database path " + db.getPath());
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        bloggerPostDao = daoSession.getBloggerPostDao();
    }

    public static Database getInstance(Context context) {
        if (instance == null) {
            instance = new Database(context);
        }
        return instance;
    }


    private boolean PostExists(BloggerPost post) {
        return bloggerPostDao.queryBuilder()
                .where(BloggerPostDao.Properties.Id.eq(post.getId())).unique() != null;
    }

    public boolean AddPostToDatabase(BloggerPost post) {
        if (!PostExists(post)) {
            long ret = bloggerPostDao.insertOrReplace(post);
            Log.d("Dao", "Inserted new post with title " + post.getTitle());
            return true;
        } else {
            Log.d("Dao", "Post exists with title:" +  post.getTitle());
            return false;
        }
    }

    public List<BloggerPost> GetPostsWithAuthorName(String authorName) {
        List posts = bloggerPostDao.queryBuilder()
                .where(BloggerPostDao.Properties.AuthorName.eq(authorName)).list();

        return posts;
    }

    public BloggerPost GetBloogerPostWithId(String postId) {
        return bloggerPostDao.queryBuilder()
                .where(BloggerPostDao.Properties.Id.eq(postId)).unique();
    }

    public long GetCountOfPostsForBlogPageId(String pageId) {
        return bloggerPostDao.queryBuilder()
                .where(BloggerPostDao.Properties.BlogPageId.eq(pageId))
                .count();
    }
    public List<BloggerPost> GetPostsForBlogPageId(String pageId, int fromPosition, int limit) {
        List posts = bloggerPostDao.queryBuilder()
                .where(BloggerPostDao.Properties.BlogPageId.eq(pageId))
                .orderDesc(BloggerPostDao.Properties.DatePublished) //latest to oldest
                .offset(fromPosition)
                .limit(limit)
                .list();
        return posts;
    }

    public List<BloggerPost>GetFavorites(int fromPosition, int limit) {
        List posts = bloggerPostDao.queryBuilder()
                .where(BloggerPostDao.Properties.IsFavorite.eq(true))
                .orderDesc(BloggerPostDao.Properties.BlogPageId) //latest to oldest
                .offset(fromPosition)
                .limit(limit)
                .list();
        return posts;
    }

    public void UpdatePost(BloggerPost post) {
        bloggerPostDao.update(post);
    }
}
