/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.utils;

import android.content.Context;
import android.content.SharedPreferences;

import apps.serk01.rbandroid.configuration.Constants;

/**
 * Created by serk01 on 09/02/15.
 */
public class PrefManager {

    private static PrefManager instance;
    private Context mContext;

    public static final String PREFERENCES = "Preferences";
    public static final String HowTosKey = "HowTos";
    public static final String StreamUrlKey = "StreamUrl";


    public PrefManager() {
    }

    public static PrefManager getInstance() {
        if (instance == null) {
            instance = new PrefManager();
        }
        return instance;
    }
    public boolean showHowToScreen(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, 0);
        return preferences.getBoolean(HowTosKey, true);
    }

    public void setShowHowToScreen(boolean value, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(HowTosKey, value);
        editor.commit();
    }

    public void setStreamUrl(String streamUrl, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(StreamUrlKey, streamUrl);
        editor.commit();
    }

    public String getStramUrl(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, 0);
        return preferences.getString(StreamUrlKey, Constants.DEFAULT_STREAM_URL);
    }

}
