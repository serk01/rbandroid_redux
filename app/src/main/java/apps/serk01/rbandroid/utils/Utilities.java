/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package apps.serk01.rbandroid.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import apps.serk01.rbandroid.R;
import apps.serk01.rbandroid.activities.MainActivity;
import apps.serk01.rbandroid.configuration.Constants;
import apps.serk01.rbandroid.daogen.BloggerPost;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import com.afollestad.materialdialogs.*;
/**
 * Created by serk01 on 05/11/14.
 */
public class Utilities {

    private static final String TAG = Utilities.class.getCanonicalName();
    private static Utilities instance;


    public Utilities() {
    }

    public static Utilities getInstance() {
        if (instance == null) {
            instance = new Utilities();
        }
        return instance;
    }

    public String GetBlogPageIdForFragment(Constants.FragmentName fragmentName) {

        switch (fragmentName) {
            case kFragMain:
                return Constants.mainID;
            case kFragNews:
                return Constants.newsID;
            case kFragEkpombes:
                return Constants.ekpobesID;
            case kFragBlog:
                return Constants.blogID;
        }

        return null;
    }

    public String encodeURL(String url) {
        try {
            return URLEncoder.encode(url, "utf-8").toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "";
    }

    public String GetBlogPageTitleForFragment(Context context, Constants.FragmentName fragmentName) {
        switch (fragmentName) {
            case kFragLive:
                return context.getString(R.string.title_radiobubble);
            case kFragMain:
                return context.getString(R.string.title_main);
            case kFragNews:
                return context.getString(R.string.title_news);
            case kFragBlog:
                return context.getString(R.string.title_blog);
            case kFragEkpombes:
                return context.getString(R.string.title_ekpobes);
            case kFragFavorites:
                return context.getString(R.string.title_favorites);
        }
        return "";
    }

    public String GetNextPageToken(ArrayList<BloggerPost> postsArray) {
        return postsArray.get(postsArray.size() - 1).getNextPageToken();
    }

    public String GetVerySimpleDateFormat(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);

    }

    public void showCroutonWithMessage(String message, Activity context) {
        Crouton.makeText(context, message, Style.ALERT).show();

    }
    public void spawnChooser(Context context, String text, String extraSubject) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, extraSubject);
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.share)));
    }


    public boolean isAppInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        boolean installed = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            installed = false;
        }
        return installed;
    }

    public void makeNotification(Context context, String text, Constants.Notif_IDs notifID) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("rbandroid")
                .setContentText(text);

        Intent resultIntent = null;

        switch (notifID) {
            case kStreamNotification:
                resultIntent = new Intent(context, MainActivity.class);
                break;
            case kUpdateNotification:
                String message = String.format(context.getResources().getString(R.string.update_available), text);
                builder.setContentText(message);
//                        .setAutoCancel(true); //remove from notif bar when tapped.

                resultIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName()));
                break;
        }

        if (resultIntent != null) {
            // The stack builder object will contain an artificial back stack for the
            // started Activity.
            // This ensures that navigating backward from the Activity leads out of
            // your application to the Home screen.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

            //        // Adds the back stack
            stackBuilder.addParentStack(MainActivity.class);
            //
            //        // Adds the Intent to the top of the stack
            stackBuilder.addNextIntent(resultIntent);

            // Gets a PendingIntent containing the entire back stack
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(notifID.ordinal(), PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentIntent(resultPendingIntent);

            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(notifID.ordinal(), builder.build());
        }
    }

    public NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    public boolean isConnectedWifi(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    public boolean isConnectedMobile(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    public String getAppVersion(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String appVersion = pInfo.versionName;
            return appVersion;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean hasUpdate(Context context, String latestVersion) {
        String appVersion = getAppVersion(context);

        if (appVersion != null) {
            Log.d(TAG, "app version " + appVersion);
            Log.d(TAG,"latest version " + latestVersion);


            String[] latestVersionLevels = latestVersion.split("\\.");
            String[] appVersionLevels = appVersion.split("\\.");

            int latestMajor = 0;
            int latestMinor = 0;
            int latestRevision = 0;

            int appMajor = 0;
            int appMinor = 0;
            int appRevision = 0;


            try {
                latestMajor = Integer.parseInt(latestVersionLevels[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                latestMinor = Integer.parseInt(latestVersionLevels[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                latestRevision = Integer.parseInt(latestVersionLevels[2]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                appMajor = Integer.parseInt(appVersionLevels[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                appMinor = Integer.parseInt(appVersionLevels[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                appRevision = Integer.parseInt(appVersionLevels[2]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Boolean hasMajorUpdate = latestMajor > appMajor;
            Boolean majorsAreEqual = latestMajor == appMajor;

            Boolean hasMinorUpdate = latestMinor > appMinor;
            Boolean minorsAreEqual = latestMinor == appMinor;

            Boolean hasRevisionUpdate = latestRevision > appRevision;
            Boolean revisionsAreEqual = latestRevision == appRevision;

            Boolean hasUpdate = false;

            if (hasMajorUpdate)
                hasUpdate = true;

            if (majorsAreEqual && hasMinorUpdate)
                hasUpdate = true;

            if (majorsAreEqual && minorsAreEqual && hasRevisionUpdate)
                hasUpdate = true;

            Log.d(TAG,"has update: " + hasUpdate);
            return hasUpdate;
        }
        return false;
    }


    public void showUpdateDialog(Context context, String newVersion) {
        new MaterialDialog.Builder(context)
                .title("title")
                .content("version is " + newVersion)
                .positiveText("Update now")
                .positiveColor(R.color.color_orange)
                .negativeText("Cancel")
                .negativeColor(Color.RED)
                .show();

    }
}
