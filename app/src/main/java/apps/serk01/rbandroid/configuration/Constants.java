/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import apps.serk01.rbandroid.confidential.Confidential;
import apps.serk01.rbandroid.R;

/**
 * Created by serk01 on 31/10/14.
 */
public interface Constants {

    /* Remember, all constants in an interface
       are "public static final" so you don't have
       to declare it. */

    enum FragmentName {kFragLive, kFragMain, kFragNews, kFragEkpombes, kFragBlog, kFragFavorites, kFragSchedule};

    enum FragmentType {kFragTypeSpeaker, kFragTypeBlogPost, kFragTypeFavorites, kFragTypeViewPager};

    enum Notif_IDs {kStreamNotification, kUpdateNotification};

//    String DEFAULT_STREAM_URL = "http://stream.radiobubble.gr:8000/stream";
//    http://ec2-54-160-35-5.compute-1.amazonaws.com:8000/stream.m3u
    String DEFAULT_STREAM_URL = "http://stream.radiojar.com/g5gmfsmp5wwtv";

    //www.radiobubble.gr page IDs
    String mainID = "115448989197646163";
    String ekpobesID = "990088310843684188";
    String newsID = "5586937803682987599";
    String blogID = "3247539740372662517";
//    String cafeBarID = This is a tumblr blog


    String BLOGGER_BASE_URL = "https://www.googleapis.com/blogger/v3/blogs/";
    String BLOGGER_BLOG_POSTS_URL = BLOGGER_BASE_URL + "%s/posts?maxResults=%s&key=" + Confidential.GOOGLE_API_KEY;
    String BLOGGER_BLOG_POSTS_URL_WITH_START_DATE = BLOGGER_BASE_URL + "%s/posts?maxResults=%s&startDate=%s&key=" + Confidential.GOOGLE_API_KEY;
    String BLOGGER_BLOG_POSTS_URL_NEXT_PAGE = BLOGGER_BASE_URL + "%s/posts?maxResults=%s&pageToken=%s&key=" + Confidential.GOOGLE_API_KEY;
    //GET https://www.googleapis.com/blogger/v3/blogs/5586937803682987599/posts?endDate=2013-01-01T00%3A00%3A00%2B00%3A00&maxResults=10&key={YOUR_API_KEY}


    String MIXLCOUD_GET_DETAULS_SUFFIX = "/player/details/?key=";
    String MIXCLOUD_APP_PACKAGE_NAME = "com.mixcloud.player";

    //Firebase keys
    String FIREBASE_VERSION_KEY = "version";
    String FIREBASE_TESTVERSION_KEY = "testversion";
    String FIREBASE_URL_KEY = "stream_url";


    //a mapping between the label that uniquely identifies a broadcast (ekpombi)
    //and a logo image for that broadcast

    Map<String, Integer> logosMap =
            Collections.unmodifiableMap(
                    /* This is really an anonymous
                       inner class - a sub-class of j.u.HashMap */
                    new HashMap<String, Integer>() {
                        {
                            //Unnamed init block //30 in total?
                            put("stin kilida tou politismou", R.drawable.istoriespolitismou_t);
                            put("Τρεις λαλούν κι όποιος χορέψει" , R.drawable.treislaloun_t);
                            put("Κενά Αέρος", R.drawable.kena_aeros_t);
                            put("χαμηλή αυτοεκτίμηση", R.drawable.xamiliautopepithisi_t);
                            put("spin like nuts", R.drawable.spinnuts_t);
                            put("danger.few!", R.drawable.dangerfew_t);
                            put("h epoxh ton dolofonon", R.drawable.epoxidolofonon_t);
                            put("Μάλλον Ακίνδυνος", R.drawable.galaxyarchis_t);
                            put("antistaseis radiobubble", R.drawable.antistaseis_t);
                            put("εκτός εποχής", R.drawable.ektosepoxis_t);
                            put("radioerasitexnis", R.drawable.radioerasitexnis_t);
                            put("radio_sociale", R.drawable.radiosociale_t);
                            put("Διεθνής Αμνηστία", R.drawable.amnesty_t);
                            put("Globaltica", R.drawable.globaltica_t);
                            put("Ιστορίες", R.drawable.istories_t);
                            put("Commons - sounds of collective being", R.drawable.commons_t);
                            put("I Can Be A Frog", R.drawable.icanbefrog_t);
                            put("μουσικές ανασκοπήσεις", R.drawable.mousikesanaskopisis_t);
                            put("subradio", R.drawable.subradio_t);
                            put("Η Χώρα του Ποτέ Ποτέ", R.drawable.xamenapaidia_t);
                            put("χαμένο επεισόδιο", R.drawable.xamenoepisodio_t);
                            put("Aventinus και Bach", R.drawable.aventinusbach_t);
                            put("για λόγους αυστηρά προσωπικούς", R.drawable.prosopikoilogoi_t);
                            put("agit pop", R.drawable.agitpop_t);
                        }

                    });
}