/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import apps.serk01.rbandroid.daogen.BloggerPost;
import apps.serk01.rbandroid.utils.Utilities;
import apps.serk01.rbandroid.R;

/**
 * Created by serk01 on 06/11/14.
 */
public class PostListAdapter extends ArrayAdapter<BloggerPost> {

    private ArrayList<BloggerPost> mPostsArray;
    private Context mContext;
    private int mResourceFile;

    public PostListAdapter(Context context, int resource, ArrayList<BloggerPost> objects) {
        super(context, resource, objects);
        this.mPostsArray = objects;
        this.mContext = context;
        this.mResourceFile = resource;
    }

    static class ViewHolder {
        public TextView title;
        public TextView author;
        public TextView date;

    }
    @Override
    public int getCount() {

        return mPostsArray.size();
    }

    @Override
    public BloggerPost getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(mResourceFile, parent, false);

            ViewHolder viewHolder = new ViewHolder();

            viewHolder.title = (TextView) rowView.findViewById(R.id.postTitle);
            viewHolder.author = (TextView) rowView.findViewById(R.id.postAuthor);
            viewHolder.date = (TextView) rowView.findViewById(R.id.postDate);
//            NetworkImageView avatar = (NetworkImageView)rowView.findViewById(R.id.avatar);
            rowView.setTag(viewHolder);
        }

        //fill the data
        BloggerPost post = mPostsArray.get(position);
        ViewHolder viewHolder = (ViewHolder)rowView.getTag();

        viewHolder.title.setText(post.getTitle());
        viewHolder.author.setText(post.getAuthorName());
        viewHolder.date.setText(Utilities.getInstance().GetVerySimpleDateFormat(post.getDatePublished()));
//        avatar.setImageUrl(post.getAuthorImageUrl(), VolleySingleton.getInstance(mContext).getImageLoader());

        return rowView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }





}
