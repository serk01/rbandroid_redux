/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.nirhart.parallaxscroll.views.ParallaxListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import apps.serk01.rbandroid.R;
import apps.serk01.rbandroid.activities.DetailsActivity;
import apps.serk01.rbandroid.adapters.PostListAdapter;
import apps.serk01.rbandroid.configuration.Constants;
import apps.serk01.rbandroid.daogen.BloggerPost;
import apps.serk01.rbandroid.database.Database;
import apps.serk01.rbandroid.listeners.WebResponseListener;
import apps.serk01.rbandroid.net.NetOperations;
import apps.serk01.rbandroid.utils.PrefManager;
import apps.serk01.rbandroid.utils.Utilities;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public abstract class BaseListFragment extends BaseFragment implements WebResponseListener {

    private static final String TAG = BaseFragment.class.getCanonicalName();

    protected ParallaxListView mListView;
    protected SwipeRefreshLayout mSwipeLayout;
    protected PostListAdapter mListAdapter;
    protected String mBlogPageId;
    protected RelativeLayout mFooterView;
    protected Button mFooterButton;
    protected Boolean isFooterAdded;
    protected ProgressBar mGetMoreProgressIndicator;
    protected Boolean mResetListStartPosition = false;
    protected ProgressBar mListPageProgress;
    protected View mHowToView;
    ArrayList<BloggerPost> mPostsArray = new ArrayList<BloggerPost>();


    public BaseListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_listview, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        super.onButtonPressed(uri);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mBlogPageId = Utilities.getInstance().GetBlogPageIdForFragment(fragmentNameIndex);
        mHowToView = (View)getView().findViewById(R.id.howtoview);
        setup();

        if (PrefManager.getInstance().showHowToScreen(getActivity()) && fragmentType != Constants.FragmentType.kFragTypeFavorites)
            mHowToView.setVisibility(View.VISIBLE);

        updateList(true, 0, false);

    }

    protected AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            BloggerPost post = mPostsArray.get(i);
            Bundle extras = new Bundle();
            extras.putString("postId", post.getId());
            intent.putExtras(extras);
            intent.putExtra("fragmentType", fragmentNameIndex.ordinal());
            startActivity(intent);
        }
    };

    private SwipeRefreshLayout.OnRefreshListener swipeListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            if (mHowToView.getVisibility() == View.VISIBLE) {
                PrefManager.getInstance().setShowHowToScreen(false, getActivity());
                mHowToView.setVisibility(View.GONE);
            }

            if (Utilities.getInstance().isConnected(getActivity())) {
                Log.d(TAG, "Refreshing");
                mResetListStartPosition = true;
                getPostsFromNetwork(null);
            } else {
                Utilities.getInstance().showCroutonWithMessage(getResources().getString(R.string.noconnection), getActivity());
                mSwipeLayout.setRefreshing(false);
            }
        }
    };

    protected void setup() {

        mSwipeLayout = (SwipeRefreshLayout)getView().findViewById(R.id.swipeLayout);
        mSwipeLayout.setProgressBackgroundColor(R.color.color_subtitle_grey);
        mSwipeLayout.setColorSchemeResources(R.color.color_orange,
                R.color.color_orange,
                R.color.color_orange,
                R.color.color_orange);
        mSwipeLayout.setDistanceToTriggerSync(150);

        mSwipeLayout.setOnRefreshListener(swipeListener);

        mListView = (ParallaxListView)getView().findViewById(R.id.postList);
        mListView.setFastScrollEnabled(true);
        mListView.setOnItemClickListener(listItemClickListener);

        mListAdapter = new PostListAdapter(getActivity(), R.layout.row_posts_list_parallax, mPostsArray);
        mListView.setAdapter(mListAdapter);

        mListPageProgress = (ProgressBar)getView().findViewById(R.id.list_page_progress);

        isFooterAdded = false;
    }

    //each child should implement its own
    protected abstract ArrayList getResultsFromDb(int startPosition, int limit);

    protected void fetchNextPageData() {
        String nextPageToken = Utilities.getInstance().GetNextPageToken(mPostsArray);
        getPostsFromNetwork(nextPageToken);
    }

    private boolean hasMoreInDb() {
        long count = Database.getInstance(getActivity()).GetCountOfPostsForBlogPageId(mBlogPageId);
        return count > mPostsArray.size();
    }

    protected void updateList(boolean fromDb, int newCount, boolean updateFooter) {

        //if we got more in the database update the list from the local store
        if (fromDb) {
            int startPosition = mResetListStartPosition ? 0 : mPostsArray.size();
            int limit = Database.FETCH_LIMIT;

            //if startPosition is 0 then we are either on first run or we want to reload
            //the list because more recent entries have been published on the blog
            if (startPosition == 0) {
                if (mPostsArray.size() > 0)
//                    set limit to the existing array's size and add the count of newly fetch
//                    objects so the list will be reset to its exact previous state (i.e. length
//                    plus with the extra new objects
                    limit = mPostsArray.size() + newCount;

                mPostsArray.clear();
                mListAdapter.notifyDataSetChanged();
            }

            mResetListStartPosition = false;
            ArrayList<BloggerPost> postsFromDB = getResultsFromDb(startPosition, limit);

            for (Iterator<BloggerPost> it = postsFromDB.iterator(); it.hasNext(); ) {
                BloggerPost post = it.next();
                Log.d(TAG, "new post from db with title " + post.getTitle());
                mPostsArray.add(post);
            }


//            make sure we have new results to populate the list with.
//            i.e. on the very first run, this will not be the case, so we'll need to fetch data
//            from the network

            if (postsFromDB.size() > 0) {
                mListAdapter.notifyDataSetChanged();
                if (updateFooter)
                    updateFooterView(false);

                //hide the page progress indicator
//                if (mListPageProgress != null && mListPageProgress.getVisibility() == View.VISIBLE)
                hideProgressIndicator();


            }
            else if (mHowToView.getVisibility() == View.GONE && fragmentType != Constants.FragmentType.kFragTypeFavorites) {
                showProgressIndicator();
                getPostsFromNetwork(null);
            }
        }

        //otherwise fetch next page data
        else {
            fetchNextPageData();
        }
    }

    protected void hideProgressIndicator() {
        if (mListPageProgress.getVisibility() == View.VISIBLE)
            mListPageProgress.setVisibility(View.INVISIBLE);
    }

    protected void showProgressIndicator() {
        if (mListPageProgress.getVisibility() == View.GONE)
            mListPageProgress.setVisibility(View.VISIBLE);
    }

    protected void addFooterView() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mFooterView = (RelativeLayout) inflater.inflate(R.layout.button_list_footerview, null, false);
        mFooterButton = (Button)mFooterView.findViewById(R.id.more_button);
        mGetMoreProgressIndicator = (ProgressBar)mFooterView.findViewById(R.id.get_more_progress_indicator);

        mFooterButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //get next set of data
                updateList(hasMoreInDb(), 0, true);
            }
        });
        mListView.addFooterView(mFooterView);
        isFooterAdded = true;

        //on some Android versions you need to all setAdapter for the footer to show
        mListView.setAdapter(mListAdapter);

    }

    protected void updateFooterView(boolean showProgress) {

        if (!isFooterAdded) {
            addFooterView();
        }

        if (isFooterAdded) {
            if (mFooterView != null) {
                mGetMoreProgressIndicator.setVisibility(showProgress ? View.VISIBLE : View.GONE);

                //change footer view's text depending on whether there are more entries in the db or not
                if (hasMoreInDb()) {
                    mFooterButton.setText(getResources().getString(R.string.next_page));
                } else {
                    String nextPageToken = Utilities.getInstance().GetNextPageToken(mPostsArray);
                    if (nextPageToken == null)
                        mListView.removeFooterView(mFooterView);
                    else {
                        mFooterButton.setText(getResources().getString(R.string.get_more_from_net));
                    }
                }
            }
        }
    }

    protected void getPostsFromNetwork(String nextPageToken) {

        if (Utilities.getInstance().isConnected(getActivity())) {

//            if there is a next page token then we are getting past posts
            if (nextPageToken != null)
                NetOperations.getInstance(getActivity()).GetPostsForPageIdWithNextPageToken(mBlogPageId, nextPageToken, this);

//            if not, we are getting new posts
            else if (nextPageToken == null) {
                if (mPostsArray.size() > 0) {
                    try {
                        BloggerPost latestPost = mPostsArray.get(0); //get the first post in the array (i.e the latest)
                        String latestPostDate = latestPost.getDatePublishedString(); //get its published date
                        String urlEncodedDate = URLEncoder.encode(latestPostDate, "utf-8");
//                        requests posts from blogger starting with the last posts published date
                        NetOperations.getInstance(getActivity()).GetPostsForPageIdWithStartDate(mBlogPageId, urlEncodedDate, this);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                } else
                    NetOperations.getInstance(getActivity()).GetPostsForPageId(mBlogPageId, this);
            }
        } else {
            Crouton.makeText(getActivity(), R.string.noconnection, Style.ALERT).show();
        }
    }

    @Override
    public void successfullNetOperation(JSONObject response, NetOperations.REQUEST_TYPE requestType) {
        if (isAdded()) {
            int newCount = 0;

            try {
                JSONArray items = (JSONArray) response.get("items");
                String nextPageToken = null;
                try {
                    nextPageToken = response.getString("nextPageToken");
                } catch (JSONException e) {
                    Log.d(TAG, "There is no new page to get data from");
                }

                String pageTitle = Utilities.getInstance().GetBlogPageTitleForFragment(getActivity(), fragmentNameIndex);
                for (int i = 0; i < items.length(); i++) {
                    BloggerPost post = new BloggerPost();
                    post.setFieldsFromJSONObject(items.getJSONObject(i), pageTitle, nextPageToken);
                    Boolean inserted = Database.getInstance(getActivity()).AddPostToDatabase(post);
                    if (inserted)
                        newCount += 1;
                }

                mSwipeLayout.setRefreshing(false);

                //if new data downloaded, load it from database
                if (newCount > 0) {
                    updateList(true, newCount, true);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            ;
        }
    }

    @Override
    public void failedNetOperation(VolleyError error) {

    }
}
