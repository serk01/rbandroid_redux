/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import apps.serk01.rbandroid.database.Database;
import apps.serk01.rbandroid.listeners.WebResponseListener;
import apps.serk01.rbandroid.R;

/**
 * Created by serk01 on 02/11/14.
 */
public class FavoritesListFragment extends BaseListFragment implements WebResponseListener {

    private static final String TAG = FavoritesListFragment.class.getCanonicalName();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_listview, container, false);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {

        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateList(true, 0, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        hideProgressIndicator();

    }

    @Override
    protected void setup() {
        super.setup();
        mSwipeLayout.setEnabled(false);

    }

    @Override
    protected void updateList(boolean fromDb, int newCount, boolean updateFooter) {
        mResetListStartPosition = true;
        super.updateList(fromDb, newCount, false);
    }

    @Override
    protected void fetchNextPageData() {
        super.fetchNextPageData();
    }

    @Override
    protected ArrayList getResultsFromDb(int startPosition, int limit) {
        ArrayList aList = (ArrayList) Database.getInstance(getActivity()).GetFavorites(startPosition,limit);
        return aList;
    }
}
