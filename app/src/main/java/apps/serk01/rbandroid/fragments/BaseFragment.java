/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package apps.serk01.rbandroid.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import apps.serk01.rbandroid.activities.MainActivity;
import apps.serk01.rbandroid.configuration.Constants;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link apps.serk01.rbandroid.fragments.BaseFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link apps.serk01.rbandroid.fragments.BaseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BaseFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    protected static final String FRAGMENT_TYPE_ARG = "fragment type";
    protected static final String FRAGMENT_NAME = "fragment name";

    // TODO: Rename and change types of parameters
    public Constants.FragmentName fragmentNameIndex;
    public Constants.FragmentType fragmentType;

    private OnFragmentInteractionListener mListener;

    public BaseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * Using reflection with the className parameter dynamically
     * create the appropriate fragment instance
     *
     * @param fragmentNameIndex this fragment's type.
     * @param className the class name of the required fragment
     * @return A new instance of fragment.
     */


    public static BaseFragment newInstance(int fragmentNameIndex, Constants.FragmentType fragmentType, Class className) {
        BaseFragment fragment = null;
        try {
            fragment = (BaseFragment)className.newInstance();
            fragment.fragmentNameIndex = Constants.FragmentName.values()[fragmentNameIndex];
            fragment.fragmentType = fragmentType;
            Bundle args = new Bundle();
            args.putInt(FRAGMENT_TYPE_ARG, fragmentNameIndex);
            fragment.setArguments(args);
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            fragmentNameIndex = Constants.FragmentName.values()[getArguments().getInt(FRAGMENT_TYPE_ARG)];
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //children of this class should override this method and provide their own layout
        return null;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(FRAGMENT_TYPE_ARG));
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}

