/*
 * Radiobubble Android [rbandroid]. An Android app for the radiobubble community.
 * The website can be found at http://www.radiobubble.gr
 *
 *     Copyright (C) 2014  Serko Katsikian
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package apps.serk01.rbandroid.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;

import apps.serk01.rbandroid.R;
import apps.serk01.rbandroid.activities.MainActivity;
import apps.serk01.rbandroid.configuration.Constants;
import apps.serk01.rbandroid.listeners.MediaControllerEventsListener;
import apps.serk01.rbandroid.mediaplayer.LiveStreamController;
import apps.serk01.rbandroid.utils.Utilities;

/**
 * Created by serk01 on 02/11/14.
 */
public class SpeakerFragment extends BaseFragment implements MediaControllerEventsListener {

    private static final String TAG = SpeakerFragment.class.getCanonicalName();
    private ImageButton mSpeakerImage;
    private Button mSpeakerButton;
//    private ProgressBar mProgressBar;
    private ProgressWheel mProgressWheel;
    private boolean mIsPreparing;
    private boolean mIsPlaying;
    private TextView mVersionText;
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_speaker, container, false);
    }

    @Override
    public void onButtonPressed(Uri uri) {

        super.onButtonPressed(uri);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity.getApplicationContext();
   }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSpeakerImage = (ImageButton) getView().findViewById(R.id.speakerImage);
        mSpeakerButton = (Button) getView().findViewById(R.id.speakerTouchArea);
//        mProgressBar = (ProgressBar) getView().findViewById(R.id.speakerProgress);
        mProgressWheel = (ProgressWheel) getView().findViewById(R.id.speakerProgress);
        mVersionText = (TextView)getView().findViewById(R.id.app_version);

        mVersionText.setText(String.format("v%s",Utilities.getInstance().getAppVersion(getActivity())));

        mSpeakerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleStreaming();
            }
        });
    }


    private void toggleStreaming() {
        if (Utilities.getInstance().isConnected(getActivity()))
            LiveStreamController.getInstance().toggleLiveStream(this, getActivity());
        else
            Utilities.getInstance().showCroutonWithMessage(getResources().getString(R.string.noconnection), getActivity());
    }

    @Override
    public void onDetach() {

        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        setPlayerState();
    }

    private void setPlayerState() {
        mIsPlaying = LiveStreamController.getInstance().isPlaying();
        mIsPreparing = LiveStreamController.getInstance().isPreparing();
        setSpeakerImageState();
        setProgressBarState();
    }

    private void setSpeakerImageState() {
        mSpeakerImage.setSelected(mIsPlaying ? true : false);
    }

    private void setProgressBarState() {
//        mProgressBar.setVisibility(mIsPreparing ? View.VISIBLE : View.INVISIBLE);
        mProgressWheel.setVisibility(mIsPreparing ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void isPreparing() {
        Log.d(TAG,"is preparing");
        mIsPreparing = true;
        setProgressBarState();
    }

    @Override
    public void startedStreaming() {
        Log.d(TAG, "Streaming started");
        mIsPreparing = false;
        mIsPlaying = true;

        setProgressBarState();
        setSpeakerImageState();

        Utilities.getInstance().makeNotification(mContext, "On Air", Constants.Notif_IDs.kStreamNotification);

    }

    @Override
    public void stoppedStreaming() {
       Log.d(TAG,"Streaming stopped");
        mIsPreparing = false;
        mIsPlaying = false;

        setSpeakerImageState();
        setProgressBarState();

        Utilities.getInstance().makeNotification(getActivity(), "Off Air", Constants.Notif_IDs.kStreamNotification);
    }
}
