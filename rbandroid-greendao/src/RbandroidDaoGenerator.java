import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by zer01 on 04/11/14.
 */

public class RbandroidDaoGenerator {

    private static String PACKAGE_ROOT = "../app/src/main/java/";
    private static String DAO_PACKAGE_NAME = "app.serko.rbandroid.daogen";

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1000, DAO_PACKAGE_NAME);
        schema.enableKeepSectionsByDefault();

        addBloggerPost(schema);

        // generate entities
        DaoGenerator daoGenerator = new DaoGenerator();
        daoGenerator.generateAll(schema, PACKAGE_ROOT);

        printEntities(schema);
    }

    private static void printEntities(Schema schema) {
        System.out.println(schema.getEntities());
    }

    private static void addBloggerPost(Schema schema) {
        // Blogger Post entity
        Entity bloggerPost = schema.addEntity("BloggerPost");
        bloggerPost.implementsSerializable();
        bloggerPost.addStringProperty("id").unique().primaryKey();
        bloggerPost.addStringProperty("title");
        bloggerPost.addStringProperty("content");
        bloggerPost.addStringProperty("url");
        bloggerPost.addStringProperty("authorName");
        bloggerPost.addStringProperty("authorProfileUrl");
        bloggerPost.addStringProperty("authorImageUrl");
        bloggerPost.addDateProperty("datePublished");
        bloggerPost.addStringProperty("datePublishedString");
        bloggerPost.addDateProperty("dateUpdated");
        bloggerPost.addStringProperty("dateUpdatedString");
        bloggerPost.addStringProperty("blogPageId");
        bloggerPost.addStringProperty("blogPageTitle");
        bloggerPost.addStringProperty("nextPageToken");
        bloggerPost.addBooleanProperty("hasBeenParsed");
        bloggerPost.addBooleanProperty("isFavorite");
        bloggerPost.addStringProperty("downloadLinks");
        bloggerPost.addStringProperty("mixcloudIframes");
        bloggerPost.addStringProperty("mixcloudUrl");
        bloggerPost.addStringProperty("labels");
    }

}


